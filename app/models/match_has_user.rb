class MatchHasUser < ApplicationRecord

  module Side
    HOME = 'home'
    AWAY = 'away'
    ALL = [HOME, AWAY]
  end

  belongs_to :user
  belongs_to :match

  validates :side, presence: true, inclusion: { in: Side::ALL }
  validates :match_id, presence: true
  validates :user_id, presence: true
end
