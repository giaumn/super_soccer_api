class Pair < ApplicationRecord
  belongs_to :league
  belongs_to :round
  belongs_to :match, optional: true
  belongs_to :home, class_name: 'User', optional: true
  belongs_to :away, class_name: 'User', optional: true

  validates :league_id, presence: true
  validates :round_id, presence: true
end
