class League < ApplicationRecord

  module Type
    INDIVIDUAL = 'individual'
    NATIONAL = 'national'
    ALL = [INDIVIDUAL, NATIONAL]
  end

  module Format
    CUP = 'cup'
    LEAGUE = 'league'
    ALL = [CUP, LEAGUE]
  end

	module Rank
		LEGEND = 5
		DIAMOND = 4
		PLATINUM = 3
		GOLD = 2
		SILVER = 1
		BRONZE = 0
		MAX_RANK = LEGEND
		MIN_RANK = BRONZE
		ALL = [LEGEND, DIAMOND, PLATINUM, GOLD, SILVER, BRONZE]
	end

  MAX_TEAM_FOR_LEAGUE = 24
	PROMOTION_COUNT = 4
	RELEGATION_COUNT = 4

  has_many :rounds, dependent: :destroy
  has_many :league_has_users, dependent: :destroy
  has_many :users, through: :league_has_users
  has_many :pairs, dependent: :destroy
	has_many :matched_pairs, -> { where('match_id IS NOT NULL') }, dependent: :destroy, class_name: 'Pair'

	validates :name, presence: true
	validates :country_code, presence: true
  validates :league_type, inclusion: { in: Type::ALL }
  validates :format, inclusion: { in: Format::ALL }
	validates :rank, inclusion: { in: Rank::ALL }
	validates :federation, presence: true
	validates :start_date, presence: true

	scope :national, -> { where(league_type: League::Type::NATIONAL, format: League::Format::LEAGUE) }
	scope :not_started, -> { where(started_at: nil) }
	scope :short, -> { where('current_team_count < max_team_count') }
	scope :full, -> { where('current_team_count == max_team_count') }
	scope :started, -> { where('started_at IS NOT NULL') }
	scope :in_progress, -> { where('started_at IS NOT NULL AND ended_at IS NULL') }
	scope :ended, -> { where('ended_at IS NOT NULL') }

	def country_name
		ISO3166::Country[self.country_code].iso_short_name
	end

	def time_zone
		country = ISO3166::Country[self.country_code]
		country.timezones.zones.first.name
	end

	def level
		if rank == 0
			"#{country_name}'s Championship"
		else
			"#{country_name}'s Championship (Level #{rank})"
		end
	end

	def current
		self.rounds.find_by(round_number: self.current_round)
	end

	def next
		self.rounds.find_by(round_number: self.current_round + 1)
	end

	def start_date_time
		country = ISO3166::Country[self.country_code]
		timezone = country.timezones.zones.first.name
		self.start_date.in_time_zone(timezone).beginning_of_day
	end

	def start_time
		start_hour = 20

		if self.rank >= 11000
			start_hour = 9
		else
			start_hour = start_hour - self.rank / 1000
		end

		start_minute = (self.rank % 1000) / 250 * 15

		"#{start_hour.to_s.rjust(2, '0')}:#{start_minute.to_s.rjust(2, '0')}"
	end

	def create_rounds
		rounds = generate_schedules(self.max_team_count)
		ActiveRecord::Base.transaction do
			rounds.each_with_index do |round, index|
				self.rounds.create!(
					{
						start_date: self.start_date + index.day,
						schedules: round.shuffle.as_json,
						start_time: self.start_time,
						round_number: index + 1
					}
				)
			end
		end
	end

	def create_pairs
		# if new user join league, then populate pairs if there is enough teams
		if self.max_team_count == self.current_team_count
			league_users = self.users.order(id: :asc).to_a
			ActiveRecord::Base.transaction do
				self.rounds.each do |round|
					round.schedules.each do |pair|
						round.pairs.create!(
							{
								league_id: self.id,
								home_position: pair['home'],
								away_position: pair['away'],
								home_id: league_users[pair['home'] - 1].id,
								away_id: league_users[pair['away'] - 1].id
							}
						)
					end
				end
			end
		end
	end

	def generate_schedules(team_count)
		round_count = (team_count - 1)
		match_each_round = team_count / 2
		base_team = 1
		other_teams = (2..team_count).to_a.reverse

		first_half_rounds = []

		# generate first match of every round
		(1..round_count).each do |round|
			idx = round - 1
			first_half_rounds[idx] = [] if first_half_rounds[idx].blank?
			first_half_rounds[idx][0] = {home: base_team, away: other_teams[idx]}
		end

		# rotate anti-clockwise to fill in remaining matches of every rounds
		(1..round_count).each do |round|
			round_idx = round - 1
			round_first_match_away = first_half_rounds[round_idx][0][:away]
			rotation = (other_teams - [round_first_match_away]).rotate(round_idx)

			# loop to fill in remaining matches of current round,
			# first match of current round has been populated already
			# so the total number of matches remaining is match_each_round - 1

			taken_idx = 0

			# fill in away position
			(1..(match_each_round - 1)).each do |match|
				first_half_rounds[round_idx][match] = {} if first_half_rounds[round_idx][match].blank?
				first_half_rounds[round_idx][match].merge!({away: rotation[taken_idx]})
				taken_idx += 1
			end

			# fill in home position
			(1..(match_each_round - 1)).to_a.reverse.each do |match|
				first_half_rounds[round_idx][match] = {} if first_half_rounds[round_idx][match].blank?
				first_half_rounds[round_idx][match].merge!({home: rotation[taken_idx]})
				taken_idx += 1
			end

			first_half_rounds[round_idx] = first_half_rounds[round_idx].map do |match|
				match.sort.reverse.to_h
			end
		end

		second_half_rounds = []
		first_half_rounds.each_with_index do |round, idx|
			round.each do |match|
				reverse_match = match.clone
				temp = reverse_match[:home]
				reverse_match[:home] = reverse_match[:away]
				reverse_match[:away] = temp

				second_half_rounds[idx] = [] if second_half_rounds[idx].blank?
				second_half_rounds[idx] << reverse_match
			end
		end

		rounds = first_half_rounds + second_half_rounds

		rounds.shuffle!
	end

	def start
		# check if league need to be started
		now_in_zone = Time.now.in_time_zone(self.time_zone)
		if now_in_zone >= self.start_date_time
			# Start leagues if time is right
			if self.max_team_count < self.current_team_count
				# delay the league if does not have enough player
				self.update!(start_date: self.start_date + 10)
				StartLeagueWorker.perform_at(self.start_date_time, self.id)
			else
				StartLeagueWorker.perform_async(self.id)
			end
		else
			StartLeagueWorker.perform_at(self.start_date_time, self.id)
		end
	end

	def update_extended_rounds
		# create extend matches if for some reason skipped (server down, ...)
		rounds = self.rounds.to_a
		last_round = self.rounds.order(start_date: :desc).first

		extended_count = 0
		rounds.each do |round|
			next if round.extended.present?
			next if round.start_date_time > Time.now.in_time_zone(self.time_zone)
			# started, check if any match was not played, then create extend match for it
			unmatched_pairs = round.pairs.where(match_id: nil)
			if unmatched_pairs.count > 0
				ActiveRecord::Base.transaction do
					schedules = unmatched_pairs.map do |pair|
						{home: pair.home_position, away: pair.away_position}
					end
					extended_round = self.rounds.create!(
						{
							start_date: last_round.start_date + extended_count + 1,
							schedules: schedules.shuffle.as_json,
							start_time: self.start_time,
							round_number: last_round.round_number + extended_count + 1,
							extend_of_id: round.original.present? ? round.original.id : round.id
						}
					)
					unmatched_pairs.each do |pair|
						extended_round.pairs.create!(
							{
								league_id: self.id,
								home_position: pair.home_position,
								away_position: pair.away_position,
								home_id: pair.home_id,
								away_id: pair.away_id
							}
						)
					end
				end
				extended_count = extended_count + 1
			end
		end
	end

	def standings
		teams = {}
		self.pairs.each do |pair|
			home = pair.home
			away = pair.away
			match = pair.match
			teams[home.id] = {
				match_played: 0,
				won: 0,
				loss: 0,
				draw: 0,
				goals_for: 0,
				goals_against: 0,
				goals_difference: 0,
				points: 0
			} if teams[home.id].blank?
			teams[away.id] = {
				match_played: 0,
				won: 0,
				loss: 0,
				draw: 0,
				goals_for: 0,
				goals_against: 0,
				goals_difference: 0,
				points: 0
			} if teams[away.id].blank?

			teams[home.id][:id] = home.id
			teams[home.id][:team_name] = home.team_name
			teams[away.id][:id] = away.id
			teams[away.id][:team_name] = away.team_name

			next if match.blank?

			teams[home.id][:match_played] += 1
			teams[away.id][:match_played] += 1

			if match.home_score > match.away_score
				teams[home.id][:won] += 1
				teams[away.id][:loss] += 1
				teams[home.id][:points] += 3
			elsif match.home_score == match.away_score
				if match.home_passes > 0
					teams[home.id][:draw] += 1
					teams[home.id][:points] += 1
				end

				if match.away_passes > 0
					teams[away.id][:draw] += 1
					teams[away.id][:points] += 1
				end
			else
				teams[home.id][:loss] += 1
				teams[away.id][:won] += 1
				teams[away.id][:points] += 3
			end

			if match.home_passes > 0
				teams[home.id][:goals_for] += match.home_score
				teams[home.id][:goals_against] += match.away_score
				teams[home.id][:goals_difference] = teams[home.id][:goals_for] - teams[home.id][:goals_against]
			end

			if match.away_passes > 0
				teams[away.id][:goals_for] += match.away_score
				teams[away.id][:goals_against] += match.home_score
				teams[away.id][:goals_difference] = teams[away.id][:goals_for] - teams[away.id][:goals_against]
			end
		end

		teams.map { |k, v| v}.sort_by { |team| [-team[:points], -team[:goals_difference]]}
	end

	def ended_on_season
		League.season_of(self.ended_at.in_time_zone(self.time_zone).month)
	end

	def starts_in
		(self.start_date - Date.today).to_i
	end

	def ended_days
		return if self.ended_at.blank?
		ended_days = (Date.today - self.ended_at.in_time_zone(self.time_zone).to_date).to_i
		ended_days < 0 ? 0 : ended_days
	end

	def new_season_in_days
		if League.current_season.starts_with?('Early')
			new_season_start_date = Date.today.change({month: 5, day: 1})
		elsif League.current_season.starts_with?('Mid')
			new_season_start_date = Date.today.change({month: 9, day: 1})
		else
			new_season_start_date = Date.today.change({year: Date.today.year + 1, month: 1, day: 1})
		end

		day_count = new_season_start_date - Date.today
		day_count < 0 ? 0 : day_count.to_i
	end

	def self.season_of(month)
		year = Date.today.year
		if month >= 1 && month < 5
			"Early #{year}"
		elsif month >= 5 && month < 9
			"Mid #{year}"
		else
			"Late #{year}"
		end
	end

	def self.current_season
		current_month = Date.today.month
		self.season_of(current_month)
	end

	def self.next_season
		if current_season.starts_with?('Early')
			"Mid #{Date.today.year}"
		elsif current_season.starts_with?('Mid')
			"Late #{Date.today.year}"
		else
			"Early #{Date.today.year + 1}"
		end
	end
end
