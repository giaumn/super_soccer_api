class Round < ApplicationRecord
  belongs_to :league
  belongs_to :original, foreign_key: :extend_of_id, class_name: 'Round', optional: true
  has_one :extended, foreign_key: :extend_of_id, class_name: 'Round'

  has_many :pairs, dependent: :destroy

  validates :league_id, presence: true
  validates :start_date, presence: true
  validates :schedules, presence: true

  serialize :schedules, Array

  scope :in_progress, -> { where('started_at IS NOT NULL AND ended_at IS NULL')}

  def formatted_start_date
    if start_date == Date.today
      'Today'
    elsif start_date == Date.tomorrow
      'Tomorrow'
    elsif start_date == Date.yesterday
      'Yesterday'
    else
      start_date.strftime('%A, %b %d')
    end
  end

  def start_date_time
    start_hour = self.start_time.split(':')[0]
    start_min = self.start_time.split(':')[1]
    self.start_date.to_time.in_time_zone(self.league.time_zone).change({hour: start_hour, min: start_min})
  end

  def create_extended_round
    last_round = self.league.rounds.order(start_date: :desc).first

    return if self.extended.present?
    return if self.start_date_time > Time.now.in_time_zone(self.league.time_zone)
    return if self.ended_at.blank?

    # started, check if any match was not played, then create extended match for it
    unmatched_pairs = self.pairs.where(match_id: nil)
    if unmatched_pairs.count > 0
      ActiveRecord::Base.transaction do
        schedules = unmatched_pairs.map do |pair|
          {home: pair.home_position, away: pair.away_position}
        end
        extended_round = self.league.rounds.create!(
          {
            start_date: last_round.start_date + 1,
            schedules: schedules,
            start_time: self.start_time,
            round_number: last_round.round_number + 1,
            extend_of_id: self.original.present? ? self.original.id : self.id
          }
        )
        unmatched_pairs.each do |pair|
          extended_round.pairs.create!(
            {
              league_id: self.id,
              home_position: pair.home_position,
              away_position: pair.away_position,
              home_id: pair.home_id,
              away_id: pair.away_id
            }
          )
        end
      end
    end
  end
end
