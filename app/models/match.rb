class Match < ApplicationRecord
  has_many :match_has_users, dependent: :destroy
  has_many :users, through: :match_has_users

  has_one :match_has_home, -> { where(side: MatchHasUser::Side::HOME) }, class_name: 'MatchHasUser'
  has_one :home, through: :match_has_home, source: :user

  has_one :match_has_away, -> { where(side: MatchHasUser::Side::AWAY) }, class_name: 'MatchHasUser'
  has_one :away, through: :match_has_away, source: :user

  # Win
  # 1 point ->            + 5 coins
  # 1 pass ->             + 1 coin
  # 1 tackle ->           + 2 coins
  # 1 shot ->             + 3 coins
  # 1 save ->             + 3 coins
  # more possession ->    + 4 coins
  # Lose
  # 1 point ->            + 3 coins
  # 1 pass ->             + 0.5 coin
  # 1 tackle ->           + 1 coins
  # 1 shot ->             + 2 coins
  # 1 save ->             + 2 coins
  # more possession ->    + 3 coins
  def away_rewards
    if self.away_score > self.home_score
      reward = (self.away_score * 5 +
        self.away_passes * 1 +
        self.away_tackles * 2 +
        self.away_shots * 3 +
        self.away_goal_saves * 3 +
        (self.away_possession > self.home_possession ? 4 : 0)).to_i
    else
      reward = (self.away_score * 3 +
        self.away_passes * 0.5 +
        self.away_tackles * 1 +
        self.away_shots * 2 +
        self.away_goal_saves * 2 +
        (self.away_possession > self.home_possession ? 3 : 0)).to_i
    end
    reward *= 100
  end

  def home_rewards
    if self.home_score > self.away_score
      reward = (self.home_score * 5 +
        self.home_passes * 1 +
        self.home_tackles * 2 +
        self.home_shots * 3 +
        self.home_goal_saves * 3 +
        (self.home_possession > self.away_possession ? 4 : 0)).to_i
    else
      reward = (self.home_score * 3 +
        self.home_passes * 0.5 +
        self.home_tackles * 1 +
        self.home_shots * 2 +
        self.home_goal_saves * 2 +
        (self.home_possession > self.away_possession ? 3 : 0)).to_i
    end
    reward *= 100
  end
end
