class User < ApplicationRecord
  include ActionView::Helpers::NumberHelper

  module Provider
    GUEST = 'guest'
    GOOGLE_PLAY_GAME_SERVICES = 'google_play_game_services'
    ALL = [GUEST, GOOGLE_PLAY_GAME_SERVICES]
  end

  has_many :tokens, dependent: :delete_all
  has_many :match_has_users, dependent: :delete_all
  has_many :matches, through: :match_has_users
  has_many :league_has_users, dependent: :delete_all
  has_many :leagues, -> { where(ended_at: nil) }, through: :league_has_users
  has_many :national_leagues, -> { where(league_type: League::Type::NATIONAL, format: League::Format::LEAGUE).order(created_at: :desc) }, through: :league_has_users, class_name: 'League', source: :league
  has_many :notifications
  has_many :coin_histories

  validates :provider, inclusion: { in: Provider::ALL }
  validates :uid, presence: true
  validates :name, presence: true, length: {minimum: 4, maximum: 18}, uniqueness: {case_sensitive: false}
  validates :team_name, presence: true, length: {minimum: 4, maximum: 20}, uniqueness: {case_sensitive: false}

  scope :players, -> { where('is_bot IS NULL OR is_bot = false') }
  scope :national_league_unlocked, -> { where('national_league_unlocked_at IS NOT NULL') }
  scope :waiting_for_federation, -> { left_joins(:league_has_users).players.national_league_unlocked.where('league_has_users.league_id IS NULL') }

  def days_played_in_last_7_days
    days_played_in_last_x_days(7)
  end

  def days_played_in_last_x_days(x_days)
    zone = self.timezone || 'Asia/Ho_Chi_Minh'
    result = self.matches
      .where(
        'matches.created_at >= ? AND matches.created_at <= ?',
        (x_days - 1).days.ago.in_time_zone(zone).beginning_of_day,
        Time.now.in_time_zone(zone).end_of_day
      )
      .group("to_char(matches.created_at, 'YYYY-MM-DD')")
      .having('COUNT(matches.id) > 0')
      .count
    result.keys.size
  end

  def national_league
    self.national_leagues.includes(pairs: [:home, :away, :match]).first
  end

  def cash
    number_to_currency(self.coins, unit: ' $', precision: 0, separator: ",", delimiter: ".", format: "%n%u")
  end
end
