class CoinHistory < ApplicationRecord

  module Source
    MATCH_REWARD = 'match_reward'
    ALL = [MATCH_REWARD]
  end

  belongs_to :user

  validates :source, presence: true, inclusion: { in: Source::ALL }
  validates :value, numericality: {min: 0, max: 150}
end
