class PlayerSerializer < ActiveModel::Serializer
  attributes :id, :name, :team_name, :country_code, :preferred_formation, :created_at, :updated_at
end

class PairSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :home, serializer: PlayerSerializer
  belongs_to :away, serializer: PlayerSerializer
  belongs_to :match
end
