class LeagueOfRoundSerializer < ActiveModel::Serializer
  attributes :id, :name, :country_code, :level
end

class TodayRoundSerializer < ActiveModel::Serializer
  attributes :id, :formatted_start_date, :start_time, :round_number, :started_at, :ended_at, :created_at, :updated_at

  belongs_to :league, serializer: LeagueOfRoundSerializer
  belongs_to :original
  has_many :pairs
end
