class LeagueSerializer < ActiveModel::Serializer
  attributes :id, :name, :country_code, :league_type, :format, :level,
             :max_team_count, :current_team_count, :start_date, :current_round, :current,
             :started_at, :ended_at, :standings, :created_at, :updated_at

  has_many :rounds do
    object.rounds.includes(:original, pairs: [:home, :away, :match]).order(round_number: :asc)
  end
end