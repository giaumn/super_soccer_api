class MatchSerializer < ActiveModel::Serializer
  attributes :id, :home_score, :away_score, :quit_id, :home_rewards, :away_rewards
end
