class OriginalRoundSerializer < ActiveModel::Serializer
  attributes :id, :formatted_start_date, :round_number
end

class RoundSerializer < ActiveModel::Serializer
  attributes :id, :formatted_start_date, :start_time, :round_number, :started_at, :ended_at, :created_at, :updated_at

  belongs_to :original, serializer: OriginalRoundSerializer
  has_many :pairs
end
