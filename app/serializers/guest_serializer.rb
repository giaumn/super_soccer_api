class GuestSerializer < ActiveModel::Serializer
  attributes :token_type, :token_value

  def token_type
    Token::Type::BEARER
  end

  def token_value
    token = self.object.tokens.order(updated_at: :desc).first
    if token.present?
      token.token_value
    else
      nil
    end
  end
end
