class BoardUserSerializer < ActiveModel::Serializer
  attributes :name, :team_name, :country_code, :points
end
