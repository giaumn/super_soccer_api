class CurrentRoundSerializer < ActiveModel::Serializer
  attributes :id, :formatted_start_date, :start_time, :round_number
end

class NationalLeagueSerializer < ActiveModel::Serializer
  attributes :id, :max_team_count, :current_team_count, :start_date,
             :started_at, :ended_at, :standings, :starts_in, :ended_days,
             :new_season_in_days, :season

  has_one :current_round, serializer: CurrentRoundSerializer do
    object.current
  end
end

class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :team_name, :email, :provider, :uid,
             :country_code, :timezone, :preferred_formation,
             :days_played_in_last_7_days, :idle_count, :max_idle_count,
             :national_league_unlocked_at, :created_at, :updated_at, :cash

  has_one :national_league, serializer: NationalLeagueSerializer do
    object.national_league
  end

  def idle_count
    User
      .waiting_for_federation
      .where(country_code: object.country_code)
      .count
  end

  def max_idle_count
    League::MAX_TEAM_FOR_LEAGUE * League::Rank::ALL.size
  end
end
