module ResponseHandler
  extend ActiveSupport::Concern

  def json_response(object, opts = {})
    render json: {
      data: serialize(object, opts),
      success: true,
      messages: [],
      error_code: nil,
    }, status: :ok
  end

  def json_response_with_paging(data, paginate_object, opts = {})
    render json: {
      data: serialize(data, opts),
      success: true,
      messages: [],
      error_code: nil,
      paging: paginate(paginate_object)
    }, status: :ok
  end

  def paging_response(object, opts = {})
    render json: {
      data: serialize(object, opts),
      success: true,
      messages: [],
      error_code: nil,
      paging: paginate(object)
    }, status: :ok
  end

  def error_response(message = '', status = :ok, error_code: 2000)
    render json: {
      data: nil,
      success: false,
      messages: [message],
      error_code: error_code
    }, status: status
  end

  def paginate(object)
    {
      per_page: object.limit_value,
      total_count: object.total_count,
      current_page: object.current_page,
      total_pages: object.total_pages
    }
  end

  def serialize(object, opts = {})
    return {} if object.nil?
    serialization = ActiveModelSerializers::SerializableResource.new(object, opts)
    return object unless serialization.serializer?
    serialization.as_json
  end

end
