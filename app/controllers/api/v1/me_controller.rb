class Api::V1::MeController < Api::ApiController

  def show
    data = serialize(current_user)
    national_league = data[:national_league]
    if national_league.present?
      my_position = national_league[:standings].find_index { |s| s[:id] == current_user.id }
      data[:my_position] = my_position.nil? ? nil : my_position + 1
    end
    current_user.update!(last_active_at: Time.now)
    json_response(data)
  end

  def update
    data = update_params.reject { |k, v| v.blank? }
    data[:team_name] = data[:team_name].strip.upcase if data[:team_name].present?
    data[:name] = data[:name].strip.upcase if data[:name].present?
    current_user.update!(data)
    json_response(current_user)
  end

  def link
    data = link_params
    id_token = data.delete(:id_token)
    payload = GoogleUtil.parse_token(id_token)
    uid = payload['sub']
    user = User.find_by(provider: data[:provider], uid: uid)
    action = data.delete(:data_action)

    data[:email] =  payload['email']
    data[:uid] = uid
    if user.present?
      if action == 'override'
        ActiveRecord::Base.transaction do
          # unlink old user
          user.update!(
            provider: User::Provider::GUEST,
            uid: SecureRandom.uuid,
            email: nil
          )

          # link to new user
          current_user.update!(data)
          json_response(current_user)
        end
      elsif action == 'restore'
        token = user.tokens.create!(
          {
            token_type: Token::Type::BEARER,
            token_value: Jwt.encode({id: user.id}, 10.years.from_now)
          }
        )
        json_data = serialize(user)
        json_data[:token_type] = token.token_type
        json_data[:token_value] = token.token_value
        json_response(json_data)
      else
        json_response(user)
      end
    else
      current_user.update!(data)
      json_response(current_user)
    end
  end

  def national_league
    league = current_user.national_league
    json_response(league)
  end

  private

  def update_params
    params.permit(:name, :team_name, :fcm_token)
  end

  def link_params
    params.permit(:provider, :id_token, :data_action)
  end
end
