class Api::V1::MatchesController < Api::ApiController
  skip_before_action :authorize

  def create
    data = create_params
    home_id = data.delete(:home_id)
    home = User.find_by(id: home_id)

    return error_response('Home is not found') if home.blank?

    away_id = data.delete(:away_id)
    away = User.find_by(id: away_id)

    return error_response('Away is not found') if away.blank?

    pair_id = data.delete(:pair_id)

    if pair_id.present?
      pair = Pair.find_by(id: pair_id)
      return error_response('Pair is not found') if pair.blank?
    end

    ActiveRecord::Base.transaction do
      home_preferred_formation = data.delete(:home_preferred_formation)
      away_preferred_formation = data.delete(:away_preferred_formation)

      match = Match.create!(data)
      match.match_has_users.create!(user_id: home_id, side: MatchHasUser::Side::HOME)
      match.match_has_users.create!(user_id: away_id, side: MatchHasUser::Side::AWAY)

      match.home.update!(preferred_formation: home_preferred_formation)
      match.away.update!(preferred_formation: away_preferred_formation)

      pair.update!(match_id: match.id) if pair.present?

      quit_id = match.quit_id
      if quit_id == nil
        if match.home_score > match.away_score
          match.home.update!(points: match.home.points + 3)
        elsif match.home_score == match.away_score
          match.home.update!(points: match.home.points + 1)
          match.away.update!(points: match.away.points + 1)
        else
          match.away.update!(points: match.away.points + 3)
        end

        match.home.update!(coins: match.home.coins + match.home_rewards)
        match.away.update!(coins: match.away.coins + match.away_rewards)
        match.home.coin_histories.create!(source: CoinHistory::Source::MATCH_REWARD, value: match.home_rewards)
        match.away.coin_histories.create!(source: CoinHistory::Source::MATCH_REWARD, value: match.away_rewards)
      else
        if quit_id == match.home.id
          match.away.update!(points: match.away.points + 3)
          match.away.update!(coins: match.away.coins + match.away_rewards)
          match.away.coin_histories.create!(source: CoinHistory::Source::MATCH_REWARD, value: match.away_rewards)
        else
          match.home.update!(points: match.home.points + 3)
          match.home.update!(coins: match.home.coins + match.home_rewards)
          match.home.coin_histories.create!(source: CoinHistory::Source::MATCH_REWARD, value: match.home_rewards)
        end
      end
      begin
        UnlockNationalLeagueWorker.perform_async(match.id)
      rescue
        puts 'Cannot schedule sidekiq worker UnlockNationalLeagueWorker'
      end
      json_response(match)
    end
  end

  private

  def create_params
    params.permit(
      :pair_id,
      :home_id, :away_id, :quit_id,
      :home_score, :away_score,
      :home_passes, :away_passes,
      :home_tackles, :away_tackles,
      :home_shots, :away_shots,
      :home_possession, :away_possession,
      :home_goal_saves, :away_goal_saves,
      :home_preferred_formation, :away_preferred_formation
    )
  end
end
