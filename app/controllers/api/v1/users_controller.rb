class Api::V1::UsersController < Api::ApiController
  skip_before_action :authorize, only: [:create]

  def create
    ActiveRecord::Base.transaction do
      last_player = User.order(id: :desc).first
      next_player_id = 1229 + (last_player.present? ? last_player.id : 0)
      next_value = (next_player_id + 1).to_s.rjust(4, '0')

      country = Geocoder.search(create_params[:ip_address]).first
      if country.present?
        country_code = country.country
        timezone = country.data['timezone']
      else
        country_code = 'vn'
        timezone = 'Asia/Ho_Chi_Minh'
      end

      user_params = {
        provider: User::Provider::GUEST,
        uid: SecureRandom.uuid,
        name: "Coach #{next_value}",
        team_name: "#{country_code.upcase}#{next_value}",
        ip_address: create_params[:ip_address],
        country_code: country_code.downcase,
        timezone: timezone
      }
      user = User.create!(user_params)
      user.tokens.create!(
        {
          token_type: Token::Type::BEARER,
          token_value: Jwt.encode({id: user.id}, 10.years.from_now)
        }
      )
      json_response(user, serializer: GuestSerializer)
    end
  end

  private

  def create_params
    params.permit(:ip_address)
  end
end
