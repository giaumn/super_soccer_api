class Api::V1::BoardsController < Api::ApiController
  def show
    country_code = params[:country_code].downcase
    if country_code == 'global'
      users = User
                .order(points: :desc, updated_at: :desc, id: :desc)
                .page(page).per(per)
      higher_rank = User
                      .where('points > ?', current_user.points)
                      .count
      higher_rank_by_updated_at = User
                                    .where('points = ?', current_user.points)
                                    .where('updated_at >= ?', current_user.updated_at)
                                    .count
      higher_rank_by_updated_at_and_id = User
                                           .where('points = ?', current_user.points)
                                           .where('updated_at = ?', current_user.updated_at)
                                           .where('id > ?', current_user.id)
                                           .count
    else
      users = User
                .where(country_code: country_code)
                .order(points: :desc, updated_at: :desc, id: :desc)
                .page(page).per(per)
      higher_rank = User
                      .where(country_code: country_code)
                      .where('points > ?', current_user.points)
                      .count
      higher_rank_by_updated_at = User
                                    .where(country_code: country_code)
                                    .where('points = ?', current_user.points)
                                    .where('updated_at >= ?', current_user.updated_at)
                                    .count
      higher_rank_by_updated_at_and_id = User
                                           .where(country_code: country_code)
                                           .where('points = ?', current_user.points)
                                           .where('updated_at = ?', current_user.updated_at)
                                           .where('id > ?', current_user.id)
                                           .count
    end

    my_rank = higher_rank + higher_rank_by_updated_at + higher_rank_by_updated_at_and_id
    board_data = serialize(users, each_serializer: BoardUserSerializer)

    json_response_with_paging(
      {
        country_code: country_code,
        board: board_data,
        me: {
          country_code: current_user.country_code,
          rank: my_rank
        }
      },
      users
    )
  end
end
