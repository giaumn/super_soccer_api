class Api::V1::BotsController < Api::ApiController
  skip_before_action :authorize
  before_action :find_bot, only: [:lock, :release]

  def fetch_free
    free_bot = User.where(is_bot: true, is_bot_occupied: false).order(updated_at: :asc).first

    return json_response(free_bot) if free_bot.present?

    last_player = User.order(id: :desc).first
    next_player_id = 1229 + (last_player.present? ? last_player.id : 0)
    ip_v4 = Faker::Internet.public_ip_v4_address
    user_params = {
      provider: User::Provider::GUEST,
      uid: SecureRandom.uuid,
      name: "Coach#{next_player_id + 1}",
      team_name: "FC#{next_player_id + 1}",
      ip_address: ip_v4,
      country_code: Geocoder.search(ip_v4).first.country.downcase,
      is_bot: true,
      is_bot_occupied: false
    }
    bot = User.create!(user_params)
    json_response(bot)
  end

  def lock
    @bot.update!(is_bot_occupied: true)
    json_response(@bot)
  end

  def release
    @bot.update!(is_bot_occupied: false)
    json_response(@bot)
  end

  private

  def find_bot
    @bot = User.find_by(id: params[:id])
    error_response('Cannot find bot') if @bot.blank?
  end
end
