class Api::V1::RoundsController < Api::ApiController
  skip_before_action :authorize

  def in_progress
    rounds = Round.in_progress.includes(:league, pairs: [:home, :away, :match]).to_a
    filter_rounds = rounds.select do |round|
      time_zone = round.league.time_zone
      now = Time.now.in_time_zone(time_zone) - 1.hours - 42.minutes

      started_at = round.started_at.in_time_zone(time_zone)
      # only select round that has just started within 1 minute since its start time
      # and now is not too far away from round's start time (1 minute allowed)

      started_at >= round.start_date_time &&
        started_at < round.start_date_time + 1.minutes &&
        now <= round.start_date_time + 1.minutes
    end

    json_response(filter_rounds, each_serializer: TodayRoundSerializer)
  end
end
