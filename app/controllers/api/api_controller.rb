class Api::ApiController < ApplicationController
  include ResponseHandler
  include ExceptionHandler

  before_action :authorize

  attr_accessor :current_user

  protected

  def page
    params[:page].to_i.zero? ? 1 : params[:page].to_i
  end

  def per
    params[:per].to_i.zero? ? 15 : params[:per].to_i
  end

  private

  def authorize
    return error_response('Bad access.', :ok, error_code: 1000) if request.headers['Authorization'].blank?

    token_info = request.headers['Authorization'].split(' ')
    return error_response('Invalid access.', :ok,  error_code: 1001) if token_info.length != 2

    token = Token.find_by(token_type: token_info[0], token_value: token_info[1])
    return error_response('Invalid or expired access.', :ok, error_code: 1002) if token.blank?

    begin
      decoded_payload = Jwt.decode(token.token_value)
      @current_user = User.find_by(id: decoded_payload[:id]) if decoded_payload
      return error_response('Access denied.', :ok, error_code: 1003) if @current_user.blank?
    rescue JWT::ExpiredSignature
      return error_response('Expired access. Please login again!', :ok, error_code: 1004)
    end
  end
end