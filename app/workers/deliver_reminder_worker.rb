class DeliverReminderWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(user_ids, title, body, icon, tag)
    return if user_ids.size == 0

    users = User.where(id: user_ids).to_a

    return if users.size == 0

    Fcm.send_to(users, title, body, icon, tag)
  end
end