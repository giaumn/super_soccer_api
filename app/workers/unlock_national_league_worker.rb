class UnlockNationalLeagueWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    match = Match.find(id)

    # unlock national league if played 7 days in a row
    ActiveRecord::Base.transaction do
      home = match.home
      if home.days_played_in_last_x_days(7) >= 7
        home.update!(national_league_unlocked_at: Time.now)
        assign_to_league(home)
      end

      away = match.away
      if away.days_played_in_last_x_days(7) >= 7
        away.update!(national_league_unlocked_at: Time.now)
        assign_to_league(away)
      end
    end
  end

	def assign_to_league(user)
    max_team_in_a_federation = League::MAX_TEAM_FOR_LEAGUE * League::Rank::ALL.size
    unlocked = User
                 .waiting_for_federation
                 .where(country_code: user.country_code)
                 .order(national_league_unlocked_at: :asc)
                 .limit(max_team_in_a_federation)
                 .to_a

    if unlocked.size == max_team_in_a_federation
      # enough 24 teams for 6 different ranks, split them base on performance of matches
      ActiveRecord::Base.transaction do
        last_league_by_federation = League.national.order(federation: :desc).first
        next_federation = last_league_by_federation.present? ? last_league_by_federation.federation + 1 : 0
        unlocked
          .sort_by { |u| [u.points, -u.id] }
          .in_groups_of(League::MAX_TEAM_FOR_LEAGUE).each_with_index do |group, index|
            new_league = League.create!(
              {
                name: "National League #{League.current_season}",
                country_code: user.country_code,
                league_type: League::Type::NATIONAL,
                format: League::Format::LEAGUE,
                rank: index,
                federation: next_federation,
                max_team_count: League::MAX_TEAM_FOR_LEAGUE,
                current_team_count: group.size,
                start_date: Date.today + 7,
                current_round: 1,
                season: League.current_season
              }
            )
            group.each do |lu|
              new_league.league_has_users.create!(user_id: lu.id)
            end
            new_league.start
        end
      end
    end
  end
end