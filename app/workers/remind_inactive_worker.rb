class RemindInactiveWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(days)
    days_ago = Time.now - (days - 0.5).days
    inactive_users = User.where('last_active_at <= ? AND reminded_count IS NULL', days_ago.utc)
    inactive_users_by_countries = inactive_users.group_by(&:country_code)
    inactive_users_by_countries.each do |country_code, users|
      country = ISO3166::Country[country_code]
      time_zone = country.timezones.zones.first.name

      deliver_time = Date.tomorrow.in_time_zone(time_zone).change({hour: Random.rand(2..4), min: Random.rand(0..59)})
      messages = [
        'What happen? Still remember how to play?',
        'Stop drifting in the board, please!',
        'Long day? Tap to play soccer with foreigners!'
      ]

      DeliverReminderWorker.perform_at(
        deliver_time,
        users.map(&:id),
        'You have not played for a while',
        messages[Random.rand(0..(messages.size - 1))],
        'https://www.memecreator.org/static/images/memes/5451916.jpg',
        'inactive_reminder'
      )
    end

    inactive_users.update_all(reminded_count: 1)
  end
end