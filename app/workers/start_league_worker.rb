class StartLeagueWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    league = League.find(id)
    if league.started_at.blank?
      league.update!(started_at: Time.now.in_time_zone(league.time_zone))
      league.create_rounds
      league.create_pairs
    end

    # extra work to create matches that didn't played for specific reasons
    # eg: (server down, api call after match ended is failed, ...)
    league.update_extended_rounds

    date_in_zone = Time.now.in_time_zone(league.time_zone).to_date
    start_date_in_zone = league.start_date.in_time_zone(league.time_zone).to_date
    date_diff = date_in_zone.mjd - start_date_in_zone.mjd

    # this is likely never going to happen
    return if date_diff < 0

    candidate_round_number = date_diff + 1

    prev_round_number = candidate_round_number - 1

    if prev_round_number > 0
      prev_round = league.rounds.find_by(round_number: prev_round_number)
      if prev_round.present?
        prev_round_started_at = prev_round.started_at
        if prev_round_started_at.present?
          # candidate round was started sometime in the past, fulfill its ending logic
          EndRoundWorker.perform_at(prev_round.start_date_time.end_of_day, prev_round.id)
        end
      end
    end

    candidate_round = league.rounds.find_by(round_number: candidate_round_number)

    return if candidate_round.blank?

    if candidate_round.start_date_time <= Time.now.in_time_zone(league.time_zone)
      # candidate round has passed its start time
      candidate_round_started_at = candidate_round.started_at
      if candidate_round_started_at.present?
        # candidate round was started sometime in the past, fulfill its ending logic
        EndRoundWorker.perform_at(candidate_round.start_date_time.end_of_day, candidate_round.id)
      end

      # use one more further round as current round
      candidate_round_number = candidate_round_number + 1
      candidate_round = league.rounds.find_by(round_number: candidate_round_number)
      if candidate_round.present?
        league.update!(current_round: candidate_round_number)
        RemindRoundWorker.perform_at(candidate_round.start_date_time - 5.minutes, candidate_round.id)
        StartRoundWorker.perform_at(candidate_round.start_date_time, candidate_round.id)
      end
    else
      # candidate round has not come to start time
      league.update!(current_round: candidate_round_number)
      RemindRoundWorker.perform_at(candidate_round.start_date_time - 5.minutes, candidate_round.id)
      StartRoundWorker.perform_at(candidate_round.start_date_time, candidate_round.id)
    end
  end
end