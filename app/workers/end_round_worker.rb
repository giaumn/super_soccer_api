class EndRoundWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    round = Round.find(id)
    round.update!(ended_at: Time.now)

    # create extended round if any
    round.create_extended_round

    # check to end the league if there are no more rounds
    league = round.league
    last_round = league.rounds.order(round_number: :desc).first

    if last_round.ended_at.present?
      league.update!(ended_at: Time.now)
    end
  end
end