class StartSeasonWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    # Query all ended leagues
    ended = League
              .includes(:league_has_users, pairs: [:home, :away, :match])
              .national.started.ended.to_a

    # Many leagues have ended after previous season start, so they should be updated as previous season
    # eg: league started on 2022/02/15, so was named "Early 2022",
    # but then for many reasons ended on 2022/06/20 which is already "Mid 2022" season
    # so they should be updated to "Mid 2022"
    ended.each do |league|
      if league.season != league.ended_on_season
        league.update!(season: league.ended_on_season)
      end
    end

    leagues_by_countries = ended.group_by(&:country_code)

    ActiveRecord::Base.transaction do
      leagues_by_countries.each do |country_code, leagues_by_federations|
        leagues_by_federation = leagues_by_federations.group_by(&:federation)
        leagues_by_federation.each do |federation, leagues|
          leagues_by_ranking = leagues.sort_by { |league| -league.rank}
          leagues_by_ranking.each do |league|
            # perform promotion & relegation
            league_user_ids = league.standings.map { |st| st[:id] }
            if league.rank == League::Rank::MAX_RANK
              # relegate last 4 teams
              league_user_ids.pop(League::RELEGATION_COUNT)
              # promote first 4 teams of lower rank
              lower_rank_league = leagues_by_ranking.reverse[league.rank - 1]
              lower_rank_user_ids = lower_rank_league.standings.map { |st| st[:id] }
              # no relegation from higher rank since this is top rank
              higher_rank_user_ids = []
            elsif league.rank == League::Rank::MIN_RANK
              # promote first 4 teams
              league_user_ids.shift(League::PROMOTION_COUNT)
              # relegate last 4 teams of higher rank
              higher_rank_league = leagues_by_ranking.reverse[league.rank + 1]
              higher_rank_user_ids = higher_rank_league.standings.map { |st| st[:id] }
              # no promotion from lower rank since this is lowest rank
              lower_rank_user_ids = []
            else
              # promote first 4 teams
              league_user_ids.shift(League::PROMOTION_COUNT)
              # relegate last 4 teams of higher rank
              higher_rank_league = leagues_by_ranking.reverse[league.rank + 1]
              higher_rank_user_ids = higher_rank_league.standings.map { |st| st[:id] }
              # relegate last 4 teams of current rank
              league_user_ids.pop(League::RELEGATION_COUNT)
              # promote first 4 teams of lower rank
              lower_rank_league = leagues_by_ranking.reverse[league.rank - 1]
              lower_rank_user_ids = lower_rank_league.standings.map { |st| st[:id] }
            end
            new_league_user_ids = higher_rank_user_ids.last(League::RELEGATION_COUNT) + league_user_ids + lower_rank_user_ids.first(League::PROMOTION_COUNT)
            create_new_season_of(league, new_league_user_ids)
          end
        end
      end
    end
  end

	def create_new_season_of(league, league_user_ids)
    new_league = League.create!(
      {
        name: "National League #{League.next_season}",
        country_code: league.country_code,
        league_type: league.league_type,
        format: league.format,
        rank: league.rank,
        federation: league.federation,
        max_team_count: League::MAX_TEAM_FOR_LEAGUE,
        current_team_count: league_user_ids.size,
        start_date: Date.tomorrow,
        current_round: 1,
        season: League.next_season
      }
    )

    league_user_ids.each do |user_id|
      new_league.league_has_users.create!(user_id: user_id)
    end
    new_league.start
  end
end