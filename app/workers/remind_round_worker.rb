class RemindRoundWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    round = Round.find(id)
    league = round.league
    pairs = round.pairs
    pairs.each do |pair|
      users = []
      users << pair.home
      users << pair.away
      title = "#{league.level} - #{league.season}"
      body = "Round #{round.round_number}: #{pair.home.team_name} vs #{pair.away.team_name} - in 5 minutes"
      icon = ''
      tag = 'round_reminder'
      Fcm.send_to(users, title, body, icon, tag)
    end
  end
end