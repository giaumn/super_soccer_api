class StartRoundWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(id)
    round = Round.find(id)
    round.update!(started_at: Time.now)
    EndRoundWorker.perform_at(round.start_date_time.end_of_day, round.id)

    next_round = round.league.next
    RemindRoundWorker.perform_at(next_round.start_date_time - 5.minutes, next_round.id)
    StartRoundWorker.perform_at(next_round.start_date_time, next_round.id)
  end
end