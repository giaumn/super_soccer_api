class Fcm
  def self.deliver(reg_tokens, options)
    FCM_CLIENT.send(reg_tokens, options)
  end

  def self.send_to(users, title, body, icon, tag)
    notification = {
      title: title,
      body: body,
      tag: tag,
    }
    payload = {
      mutable_content: true,
      content_available: true,
      notification: notification,
      data: {
        tag: tag,
        largeIcon: icon,
        priority: 1, # For Android <= 7.1
        showWhen: true,
        autoCancel: true,
        privacy: 'Private',
      }
    }

    tokens = users.map(&:messaging_token).compact
    response = self.deliver(tokens, payload)
    users.each do |user|
      Notification.create!(
        user_id: user.id,
        payload: payload,
        response: response[:body],
        status_code: response[:status_code]
      )
    end
  end
end
