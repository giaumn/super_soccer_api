class GoogleUtil
	def self.parse_token(token)
		validator = GoogleIDToken::Validator.new
		begin
			validator.check(token, ENV['GOOGLE_CLIENT_ID'])
		rescue GoogleIDToken::ValidationError => e
			puts e
			false
		end
	end
end