class Jwt
  def self.encode(payload, exp = 3.days.from_now)
    payload[:exp] = exp.to_i
    payload[:iat] = Time.now.to_i
    JWT.encode(payload, ENV['SECRET_KEY_BASE'])
  end

  def self.decode(token)
    body = JWT.decode(token, ENV['SECRET_KEY_BASE'], true, {})[0]
    HashWithIndifferentAccess.new body
  rescue
    nil
  end
end
