class ErrorReporter
  class << self
    def capture_exception(ex)
      if Rails.env.development?
        puts ex
      else
        Raven.capture_exception(ex)
      end
    end
  end
end
