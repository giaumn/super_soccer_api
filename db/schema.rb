# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_05_30_072210) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "coin_histories", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "source", null: false
    t.integer "value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["source"], name: "index_coin_histories_on_source"
    t.index ["user_id"], name: "index_coin_histories_on_user_id"
  end

  create_table "league_has_users", force: :cascade do |t|
    t.bigint "league_id", null: false
    t.bigint "user_id", null: false
    t.integer "points", default: 0
    t.integer "match_played", default: 0
    t.integer "won", default: 0
    t.integer "draw", default: 0
    t.integer "lost", default: 0
    t.integer "goals_for", default: 0
    t.integer "goals_against", default: 0
    t.integer "goal_diff", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["league_id", "user_id"], name: "index_league_has_users_on_league_id_and_user_id", unique: true
    t.index ["league_id"], name: "index_league_has_users_on_league_id"
    t.index ["user_id"], name: "index_league_has_users_on_user_id"
  end

  create_table "leagues", force: :cascade do |t|
    t.string "name", null: false
    t.string "country_code", null: false
    t.string "league_type", null: false
    t.string "format", null: false
    t.integer "rank", null: false
    t.integer "max_team_count", null: false
    t.integer "current_team_count", default: 0
    t.date "start_date"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "current_round"
    t.string "season"
    t.integer "federation"
    t.index ["country_code", "federation", "rank"], name: "index_leagues_on_country_code_and_federation_and_rank"
    t.index ["format"], name: "index_leagues_on_format"
    t.index ["league_type"], name: "index_leagues_on_league_type"
    t.index ["started_at", "ended_at"], name: "index_leagues_on_started_at_and_ended_at"
  end

  create_table "match_has_users", force: :cascade do |t|
    t.bigint "match_id", null: false
    t.bigint "user_id", null: false
    t.string "side"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id", "side"], name: "index_match_has_users_on_match_id_and_side", unique: true
    t.index ["match_id", "user_id", "side"], name: "index_match_has_users_on_match_id_and_user_id_and_side", unique: true
    t.index ["match_id"], name: "index_match_has_users_on_match_id"
    t.index ["user_id"], name: "index_match_has_users_on_user_id"
  end

  create_table "matches", force: :cascade do |t|
    t.integer "home_score"
    t.integer "away_score"
    t.integer "home_passes"
    t.integer "away_passes"
    t.integer "home_tackles"
    t.integer "away_tackles"
    t.integer "home_shots"
    t.integer "away_shots"
    t.float "home_possession"
    t.float "away_possession"
    t.integer "home_goal_saves"
    t.integer "away_goal_saves"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "quit_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.json "payload", default: {}
    t.json "response", default: {}
    t.string "status_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "pairs", force: :cascade do |t|
    t.bigint "league_id", null: false
    t.bigint "round_id", null: false
    t.bigint "match_id"
    t.bigint "home_id"
    t.bigint "away_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "home_position"
    t.integer "away_position"
    t.index ["away_id"], name: "index_pairs_on_away_id"
    t.index ["home_id"], name: "index_pairs_on_home_id"
    t.index ["league_id"], name: "index_pairs_on_league_id"
    t.index ["match_id"], name: "index_pairs_on_match_id"
    t.index ["round_id"], name: "index_pairs_on_round_id"
  end

  create_table "rounds", force: :cascade do |t|
    t.bigint "league_id", null: false
    t.date "start_date", null: false
    t.text "schedules"
    t.string "start_time"
    t.integer "round_number"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "extend_of_id"
    t.index ["extend_of_id"], name: "index_rounds_on_extend_of_id"
    t.index ["league_id"], name: "index_rounds_on_league_id"
    t.index ["round_number"], name: "index_rounds_on_round_number"
    t.index ["started_at", "ended_at"], name: "index_rounds_on_started_at_and_ended_at"
  end

  create_table "tokens", force: :cascade do |t|
    t.bigint "user_id"
    t.string "token_value", null: false
    t.string "token_type", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["token_value", "token_type"], name: "index_tokens_on_token_value_and_token_type", unique: true
    t.index ["user_id"], name: "index_tokens_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", null: false
    t.string "uid", null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "ip_address", null: false
    t.string "country_code", null: false
    t.string "team_name", null: false
    t.boolean "is_bot", default: false
    t.boolean "is_bot_occupied", default: false
    t.integer "points", default: 0
    t.string "preferred_formation"
    t.string "email"
    t.string "timezone"
    t.datetime "national_league_unlocked_at"
    t.string "fcm_token"
    t.datetime "last_active_at"
    t.integer "reminded_count"
    t.integer "coins", default: 0
    t.index "lower((name)::text)", name: "index_users_on_lowercase_name", unique: true
    t.index "lower((team_name)::text)", name: "index_users_on_lowercase_team_name", unique: true
    t.index ["country_code", "points", "updated_at"], name: "index_users_on_country_code_and_points_and_updated_at"
    t.index ["is_bot", "is_bot_occupied"], name: "index_users_on_is_bot_and_is_bot_occupied"
    t.index ["provider", "uid"], name: "index_users_on_provider_and_uid", unique: true
  end

  add_foreign_key "coin_histories", "users"
  add_foreign_key "league_has_users", "leagues"
  add_foreign_key "league_has_users", "users"
  add_foreign_key "match_has_users", "matches"
  add_foreign_key "match_has_users", "users"
  add_foreign_key "notifications", "users"
  add_foreign_key "pairs", "leagues"
  add_foreign_key "pairs", "matches"
  add_foreign_key "pairs", "rounds"
  add_foreign_key "pairs", "users", column: "away_id"
  add_foreign_key "pairs", "users", column: "home_id"
  add_foreign_key "rounds", "leagues"
  add_foreign_key "tokens", "users"
end
