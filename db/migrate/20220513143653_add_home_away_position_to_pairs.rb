class AddHomeAwayPositionToPairs < ActiveRecord::Migration[6.0]
  def change
    add_column :pairs, :home_position, :integer
    add_column :pairs, :away_position, :integer
  end
end
