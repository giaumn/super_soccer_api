class CreateMatches < ActiveRecord::Migration[6.0]
  def change
    create_table :matches do |t|
      t.integer :home_score
      t.integer :away_score
      t.integer :home_passes
      t.integer :away_passes
      t.integer :home_tackles
      t.integer :away_tackles
      t.integer :home_shots
      t.integer :away_shots
      t.float :home_possession
      t.float :away_possession
      t.integer :home_goal_saves
      t.integer :away_goal_saves
      t.timestamps
    end
  end
end
