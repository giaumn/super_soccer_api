class CreateLeagueHasUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :league_has_users do |t|
      t.belongs_to :league, null: false, foreign_key: {to_table: :leagues}
      t.belongs_to :user, null: false, foreign_key: {to_table: :users}
      t.integer :points, default: 0
      t.integer :match_played, default: 0
      t.integer :won, default: 0
      t.integer :draw, default: 0
      t.integer :lost, default: 0
      t.integer :goals_for, default: 0
      t.integer :goals_against, default: 0
      t.integer :goal_diff, default: 0
      t.timestamps
    end

    add_index :league_has_users, [:league_id, :user_id], unique: true
  end
end
