class AddCurrentRoundToLeagues < ActiveRecord::Migration[6.0]
  def change
    add_column :leagues, :current_round, :integer
  end
end
