class CreateCoinHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :coin_histories do |t|
      t.belongs_to :user, null: false, foreign_key: {to_table: :users}
      t.string :source, index: true, null: false
      t.integer :value, null: false
      t.timestamps
    end
  end
end
