class AlterIndexOnLeagues < ActiveRecord::Migration[6.0]
  def change
    remove_index :leagues, [:country_code, :rank]
    add_index :leagues, [:country_code, :federation, :rank]
  end
end
