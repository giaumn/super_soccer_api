class AddPreferredFormationToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :preferred_formation, :string
  end
end
