class AddIsBotToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :is_bot, :boolean, default: false
    add_column :users, :is_bot_occupied, :boolean, default: false
    add_index :users, [:is_bot, :is_bot_occupied]
  end
end
