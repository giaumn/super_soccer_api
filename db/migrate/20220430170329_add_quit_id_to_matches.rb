class AddQuitIdToMatches < ActiveRecord::Migration[6.0]
  def change
    add_column :matches, :quit_id, :integer
  end
end
