class AddIndexToUsers < ActiveRecord::Migration[6.0]
  def change
    add_index :users, [:country_code, :points, :updated_at]
  end
end
