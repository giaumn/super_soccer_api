class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.belongs_to :user, null: false, foreign_key: {to_table: :users}
      t.json :payload, default: {}
      t.json :response, default: {}
      t.string :status_code
      t.timestamps
    end
  end
end
