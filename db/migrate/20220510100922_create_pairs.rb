class CreatePairs < ActiveRecord::Migration[6.0]
  def change
    create_table :pairs do |t|
      t.belongs_to :league, null: false, foreign_key: {to_table: :leagues}
      t.belongs_to :round, null: false, foreign_key: {to_table: :rounds}
      t.belongs_to :match, null: true, foreign_key: {to_table: :matches}
      t.belongs_to :home, null: true, foreign_key: {to_table: :users}
      t.belongs_to :away, null: true, foreign_key: {to_table: :users}
      t.timestamps
    end
  end
end
