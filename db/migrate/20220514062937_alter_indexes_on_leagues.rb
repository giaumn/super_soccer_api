class AlterIndexesOnLeagues < ActiveRecord::Migration[6.0]
  def change
    remove_index :leagues, :country_code
    remove_index :leagues, :rank
    add_index :leagues, [:country_code, :rank], unique: true
  end
end
