class AddNationalLeagueUnlockedAtToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :timezone, :string, index: true
    add_column :users, :national_league_unlocked_at, :datetime, index: true
  end
end
