class AddIndexOnUsers < ActiveRecord::Migration[6.0]
  def up
    execute 'CREATE UNIQUE INDEX index_users_on_lowercase_name
             ON users USING btree (lower(name));'
    execute 'CREATE UNIQUE INDEX index_users_on_lowercase_team_name
             ON users USING btree (lower(team_name));'
  end

  def down
    execute 'DROP INDEX index_users_on_lowercase_name;'
    execute 'DROP INDEX index_users_on_lowercase_team_name;'
  end
end
