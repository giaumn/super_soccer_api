class AddRemindedAtToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :reminded_count, :integer, index: true
  end
end
