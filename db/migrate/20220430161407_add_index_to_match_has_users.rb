class AddIndexToMatchHasUsers < ActiveRecord::Migration[6.0]
  def change
    add_index :match_has_users, [:match_id, :side], unique: true
  end
end
