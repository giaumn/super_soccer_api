class AddIpCountryToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :ip_address, :string, null: false
    add_column :users, :country_code, :string, null: false, index: true
  end
end
