class AddSeasonToLeagues < ActiveRecord::Migration[6.0]
  def change
    add_column :leagues, :season, :string, index: true
  end
end
