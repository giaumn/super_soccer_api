class AddFederationToLeagues < ActiveRecord::Migration[6.0]
  def change
    add_column :leagues, :federation, :integer, index: true
  end
end
