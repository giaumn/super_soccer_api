class CreateRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :rounds do |t|
      t.belongs_to :league, null: false, foreign_key: {to_table: :leagues}
      t.date :start_date, null: false
      t.text :schedules
      t.string :start_time
      t.integer :round_number, index: true
      t.datetime :started_at
      t.datetime :ended_at
      t.timestamps
    end

    add_index :rounds, [:started_at, :ended_at]
    add_reference :rounds, :extend_of, index: true
  end
end
