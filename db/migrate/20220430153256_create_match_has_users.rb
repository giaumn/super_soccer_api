class CreateMatchHasUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :match_has_users do |t|
      t.belongs_to :match, null: false, foreign_key: {to_table: :matches}
      t.belongs_to :user, null: false, foreign_key: {to_table: :users}
      t.string :side
      t.timestamps
    end

    add_index :match_has_users, [:match_id, :user_id, :side], unique: true
  end
end
