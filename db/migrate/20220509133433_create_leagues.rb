class CreateLeagues < ActiveRecord::Migration[6.0]
  def change
    create_table :leagues do |t|
      t.string :name, null: false
      t.string :country_code, null: false, index: true
      t.string :league_type, null: false, index: true
      t.string :format, null: false, index: true
      t.integer :rank, null: false, index: true
      t.integer :max_team_count, null: false
      t.integer :current_team_count, default: 0
      t.date :start_date
      t.datetime :started_at
      t.datetime :ended_at
      t.timestamps
    end
  end
end
