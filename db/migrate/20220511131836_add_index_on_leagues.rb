class AddIndexOnLeagues < ActiveRecord::Migration[6.0]
  def change
    add_index :leagues, [:started_at, :ended_at]
  end
end
