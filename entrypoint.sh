#!/bin/bash

set -e
bundle check || bundle install --binstubs="$BUNDLE_BIN"

bundle exec rake db:environment:set RAILS_ENV=$RAILS_ENV
bundle exec rake db:migrate
bundle exec rake db:seed

bundle exec rails server -p 7000 -b 0.0.0.0

exec "$@"
