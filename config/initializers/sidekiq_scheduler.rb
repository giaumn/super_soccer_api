# config/initializers/sidekiq_scheduler.rb
require 'sidekiq'
require 'sidekiq-scheduler'

Sidekiq.configure_server do |config|
	Sidekiq.redis(&:flushdb)
	config.on(:startup) do
		SidekiqScheduler::Scheduler.instance.reload_schedule!

		Rails.application.config.after_initialize do
			puts 'Initializing leagues...'

			# Check and start all upcoming leagues if the time is right
			League.national.includes(:rounds).each(&:start)

			puts 'Completed. Starting app...'
		end
	end
end