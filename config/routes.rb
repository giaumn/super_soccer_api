require 'sidekiq/web'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get 'health-checks', to: 'health_checks#index'

	# Configure Sidekiq-specific session middleware
	Sidekiq::Web.use ActionDispatch::Cookies
	Sidekiq::Web.use ActionDispatch::Session::CookieStore, key: '_interslice_session'
	Sidekiq::Web.use Rack::Auth::Basic do |username, password|
		# Protect against timing attacks:
		# - See https://codahale.com/a-lesson-in-timing-attacks/
		# - See https://thisdata.com/blog/timing-attacks-against-string-comparison/
		# - Use & (do not use &&) so that it doesn't short circuit.
		# - Use digests to stop length information leaking (see also ActiveSupport::SecurityUtils.variable_size_secure_compare)
		ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_USERNAME'])) &
			ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(ENV['SIDEKIQ_PASSWORD']))
	end if Rails.env.production?

	mount Sidekiq::Web, at: '/sidekiq'

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resources :users, only: [:create]
      resource :me, only: [:show, :update], controller: :me do
        collection do
					get :national_league
          post :link
        end
      end
      resources :bots, only: [] do
        member do
          post :lock
          post :release
        end
        collection do
          get :fetch_free
        end
      end
      resources :matches, only: [:create]
      resources :boards, only: [:show], param: :country_code
			resources :rounds, only: [] do
			  collection do
			    get :in_progress
			  end
			end
    end
  end
end
